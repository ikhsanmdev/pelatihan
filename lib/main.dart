import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:test_flutter_app/api/favorite_meal_model.dart';
import 'package:test_flutter_app/api/list_meal.dart';

import 'layout/listview_layout.dart';
import 'layout/login_layout.dart';
import 'layout/registration_layout.dart';
import 'layout/latihan2.dart';
import 'layout/bottom_navigation_layout.dart';

//void main() {
//    final favorite = FavoriteModel();
//    runApp(
//        ScopedModel(
//            model: favorite,
//            child: MyAppWithRoute()
//        )
//    );
//}

void main() {
    runApp(BottomLayout());
}

class MyApp extends StatelessWidget {
    // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
                primarySwatch: Colors.red,
                primaryColor: Colors.red[600],
                primaryColorDark: Colors.red[700],
                brightness: Brightness.light,
            ),
            home: ListMeals()
        );
    }
}

class MyAppWithRoute extends StatelessWidget {
    // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Flutter Demo',
            theme: ThemeData(
                primarySwatch: Colors.red,
                primaryColor: Colors.red[600],
                primaryColorDark: Colors.red[700],
                brightness: Brightness.light,
            ),
            initialRoute: '/',
            routes: {
                '/': (context) => MyAppLogin(),
                '/register': (context) => MyAppRegistration(),
                '/list': (context) => MyAppListView(),
            }
        );
    }
}