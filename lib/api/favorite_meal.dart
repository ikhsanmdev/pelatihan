import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import 'favorite_meal_model.dart';

class FavoriteMeal extends StatelessWidget {

    const FavoriteMeal();

    @override
    Widget build(BuildContext context) {
        print("Favorite build called");

        return ScopedModelDescendant<FavoriteModel>(
            builder: (context, child, favorite) {
                final list = favorite.favorites;
                return ListView.builder(
                    itemCount: list.length,
                    itemBuilder: (context, position) {
                        final item = list[position];
                        return ListTile(
                            leading: Image.network(item.thumbnail, width: 38, height: 38),
                            title: Text(item.mealName),
                            trailing: IconButton(
                                icon: Icon(Icons.delete_outline),
                                onPressed: () {
                                    final model = ScopedModel.of<FavoriteModel>(context);
                                    model.remove(item);
                                }),
                        );
                    }
                );
            }
        );
    }

}