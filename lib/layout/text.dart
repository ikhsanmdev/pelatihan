import 'package:flutter/material.dart';

class MyAppText extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        // TODO: implement build
        return MaterialApp(
            title: "Flutter Demo",
            home: Scaffold(
                appBar: AppBar(
                    title: Text("This is a title"),
                ),
                body: Container(
                    decoration: BoxDecoration(color: Colors.red),
                    child: Center(
                        child: Text(
                            "Training Flutter",
                            style: TextStyle(fontSize: 32, color: Colors.white),
                        ),
                    ),
                ),
            ),
        );
    }
}
