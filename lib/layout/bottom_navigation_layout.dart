import 'package:flutter/material.dart';

class BottomLayout extends StatefulWidget {

    @override
    State<StatefulWidget> createState() {
        return _BottomLayout();
    }
}

class _BottomLayout extends State<BottomLayout> {

    final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey();

    var bottomNavSelectedIndex = 0;

    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            home: Scaffold(
                key: _scaffoldState,
                appBar: AppBar(
                    title: Text("Bottom Navigation"),
                ),
                body: Container(
                    child: Center(
                        child: Text("This is center"),
                    ),
                ),
                bottomNavigationBar: BottomAppBar(
                    shape: CircularNotchedRectangle(),
                    notchMargin: 5,
                    child:
// new Row(
//                        mainAxisSize: MainAxisSize.max,
//                        mainAxisAlignment: MainAxisAlignment.start,
//                        children: <Widget>[
//                            IconButton(
//                                icon: Icon(Icons.menu),
//                                onPressed: () {},
//                            ),
//                            IconButton(
//                                icon: Icon(Icons.search),
//                                onPressed: () {},
//                            )
//                        ],
//                    ),
                    BottomNavigationBar(
                        currentIndex: bottomNavSelectedIndex,
                        onTap: (index) {
                            setState(() {
                                bottomNavSelectedIndex = index;
                            });
                        },
                        items: [
                            BottomNavigationBarItem(
                                icon: const Icon(Icons.phone),
                                title: Text("Telepon")
                            ),
                            BottomNavigationBarItem(
                                icon: const Icon(Icons.favorite),
                                title: Text("Favorite")
                            )
                        ]),
                ),
                floatingActionButton: FloatingActionButton(
                    child: const Icon(Icons.add),
                    onPressed: () {
                        _scaffoldState.currentState.hideCurrentSnackBar();
                        _scaffoldState.currentState.showSnackBar(
                            SnackBar(
                                content: Text("Hello, I am a snackbar!"),
                                duration: Duration(seconds: 10),
                                action: SnackBarAction(label: "OK", onPressed: () {
                                    _scaffoldState.currentState.hideCurrentSnackBar();
                                }),
                            )
                        );
                    }),
                floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
            ),
        );
    }
}