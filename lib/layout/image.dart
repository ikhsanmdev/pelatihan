import 'package:flutter/material.dart';

class MyAppImage extends StatelessWidget {

    final ThemeData theme = ThemeData(
        primaryColor: Colors.green[600],
        primaryColorDark: Colors.blueGrey[700],
        accentColor: Colors.indigo,
        brightness: Brightness.light,
    );

    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            theme: theme,
            home: Scaffold(
                appBar: AppBar(
                    title: Text("Asset demo"),
                ),
                body: Container(
                    decoration: BoxDecoration(color: theme.accentColor),
                    child: Center(
                        child: Image.asset("graphics/shrine.jpg"),
                    ),
                ),
            ),
        );
    }
}
