import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'detail_meal.dart';
import 'list_meal_item.dart';
import 'models.dart';

class ListMeals extends StatefulWidget {

    const ListMeals({Key key, this.title}) : super(key: key);

    final String title;

    @override
    State<StatefulWidget> createState() => _ListMeals();
}

class _ListMeals extends State<ListMeals> with AutomaticKeepAliveClientMixin<ListMeals> {

    final List<Meal> meals = new List();
    Future<Meals> _futureMeals;

    @override
    void initState() {
        super.initState();
        _futureMeals = fetchLatestMeals();
        print("List meal initState called");
    }

    @override
    Widget build(BuildContext context) {
        print("List meal build called");
        return buildBody();
    }

    Widget buildBody() {
        if (meals.isEmpty) {
            return createFutureBuilder();
        } else {
            return buildList(meals);
        }
    }

    Widget createFutureBuilder() {
        return Container(child: Center(
            child: FutureBuilder<Meals>(
                future: _futureMeals,
                builder: (context, snapshot) {
                    if (snapshot.hasData) {
                        final data = snapshot.data.meals;
                        this.meals.addAll(data);
                        return buildList(data);
                    } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                    }

                    return CircularProgressIndicator();
                },
            )
        ));
    }

    Widget buildList(List<Meal> values) {
        return Container(child: Center(
            child: RefreshIndicator(
                child: ListView.builder(
                    itemCount: values.length,
                    itemBuilder: (context, position) {
                        final item = values[position];
                        return GestureDetector(
                            child: createListItem(item),
                            onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => DetailMeal(item, item.ingredients)
                                    )
                                );
                            },
                        );
                    }),
                onRefresh: _refreshList
            )
        ));
    }

    Widget createListItem(Meal value) {
        return ListMealItem(value);
    }

    Future<Null> _refreshList() {
        _futureMeals = fetchMealsByName();
        return fetchMealsByName().then((result) {
            setState(() {
                meals.addAll(result.meals);
            });
        });
    }

    Future<Meals> fetchLatestMeals() async {
        final response = await http.get("https://www.themealdb.com/api/json/v1/1/latest.php");
        if (response.statusCode == 200) {
            return Meals.json(json.decode(response.body));
        } else {
            throw Exception('Gagal mendapatkan data');
        }
    }

    Future<Meals> fetchMealsByName() async {
        final response = await http.get("https://www.themealdb.com/api/json/v1/1/search.php?s=chocolate");
        if (response.statusCode == 200) {
            return Meals.json(json.decode(response.body));
        } else {
            throw Exception('Gagal mendapatkan data');
        }
    }

    @override
    void updateKeepAlive() {
        print("List meals update keep alive");
    }

    @override
    bool get wantKeepAlive => true;
}
