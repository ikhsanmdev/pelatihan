import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_flutter_app/api/favorite_meal.dart';
import 'package:test_flutter_app/api/list_meal.dart';

class HomeLayout extends StatelessWidget {

    final Future<SharedPreferences> _futurePrefs = SharedPreferences.getInstance();
    final list = ListMeals();
    final favorite = FavoriteMeal();

    @override
    Widget build(BuildContext context) {
        return DefaultTabController(
            length: 2,
            child: Scaffold(
                appBar: buildAppBar(context),
                body: TabBarView(
                    children: [
                        list,
                        favorite
                    ]),
            ),
        );
    }

    Widget buildAppBar(BuildContext context) {
        return AppBar(
            title: buildAppBarTitle(),
            actions: buildAppBarActions(context),
            leading: Container(
                padding: EdgeInsets.all(5),
                child: CircleAvatar(backgroundImage: AssetImage('graphics/shrine.jpg'),),
            ),
            bottom: TabBar(
                isScrollable: true,
                indicatorColor: Colors.white,
                tabs: [
                    Tab(text: "Meals"),
                    Tab(text: "Favorites",)
                ]));
    }

    Widget buildAppBarTitle() {
        return FutureBuilder(
            future: _futurePrefs,
            builder: (context, snapshot) {
                if (snapshot.data != null) {
                    return Text("Hi, ${(snapshot.data as SharedPreferences).getString("username")}!");
                } else {
                    return Text("Home");
                }
            });
    }

    List<Widget> buildAppBarActions(BuildContext context) {
        return <Widget>[
            IconButton(
                icon: Icon(Icons.favorite),
                onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => FavoriteMeal())
                    );
                }
            )
        ];
    }
}