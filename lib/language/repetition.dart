

main() {
    print("PENGULANGAN");

    print("Contoh FOR");
    int totalPengulangan = 10;
    final iterable = Iterable.generate(totalPengulangan, (num) { return num + 1; });
    for(int item in iterable) {
        print("For item : $item");
    }
    print("--------------------");
    print("");

    print("Contoh WHILE");
    var tambah = 0;
    while(tambah < totalPengulangan) {
        print("While item : $tambah");
        tambah = tambah + 1;
    }
    print("--------------------");
    print("");

    print("Contoh DO WHILE");
    do {
        print("Do while item : $tambah");
        tambah = tambah - 1;
    } while (tambah > 0);
    print("--------------------");
}