import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_flutter_app/custom/input_base.dart';

import 'home_layout.dart';

class MyAppLogin extends StatefulWidget {
    @override
    _MyAppLogin createState() => _MyAppLogin();
}

class _MyAppLogin extends State<MyAppLogin> {

    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    final roundRadius = 5.0;
    final usernameController = new TextEditingController();
    final passwordController = new TextEditingController();
    bool isChecked = false;

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            key: _scaffoldKey,
            appBar: new AppBar(
                title: Text("Training Flutter"),
            ),
            body: SingleChildScrollView(
                child: Container(
                    color: Colors.grey,
                    padding: EdgeInsets.only(left: 16, top: 20, right: 16, bottom: 40),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                            SizedBox(height: 20),
                            Container(
                                decoration: ShapeDecoration(shape: StadiumBorder(), color: Colors.blue),
                                padding: EdgeInsets.all(8),
                                margin: EdgeInsets.only(bottom: 20),
                                child: Text(
                                    "Selamat Datang",
                                    style: TextStyle(fontSize: 24),
                                    textAlign: TextAlign.center,
                                )
                            ),
                            CustomTextField(
                                label: "Username",
                                hint: "Enter your username",
                                controller: usernameController,
                                inputType: TextInputType.emailAddress
                            ),
                            CustomTextField(
                                label: "Password",
                                hint: "Enter your password",
                                controller: passwordController,
                                isPassword: true,
                            ),
                            CheckboxListTile(
                                value: isChecked,
                                title: Text("save password"),
                                activeColor: Colors.red,
                                onChanged: (changed) {
                                    setState(() {
                                        isChecked = changed;
                                    });
                                    _scaffoldKey.currentState.hideCurrentSnackBar();
                                    _scaffoldKey.currentState.showSnackBar(
                                        SnackBar(content: Text("Checkbox ${isChecked ? "checked" : "unchecked"}"))
                                    );
                                }),
                            Container(height: 20,),
                            CustomRaisedButton(
                                text: "LOGIN",
                                listener: () {
                                    _loginTapped(context: context);
                                }),
                            CustomRaisedButton(
                                text: "REGISTER",
                                listener: () {
                                    _registerTapped(context: context);
                                })
                        ],
                    ))
            ),
        );
    }

    void _loginTapped({context: BuildContext}) async {
        final username = usernameController.text;
        final password = passwordController.text;
        if (!(username.isEmpty || password.isEmpty)) {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            await prefs.setString("username", username);
            if (isChecked) {
                await prefs.setString("password", password);
            }
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeLayout()),
                    (route) => false
            );
        } else {
            _scaffoldKey.currentState.showSnackBar(
                SnackBar(
                    content: Text("Username dan password tidak boleh kosong"),
                    duration: Duration(seconds: 10),
                    action: SnackBarAction(label: "OK", onPressed: () {
                        _scaffoldKey.currentState.hideCurrentSnackBar();
                    }),
                )
            );
        }
    }

    void _registerTapped({context: BuildContext}) {
        Navigator.of(context).pushNamed("/register");
        print("Yor have tapped me!\n${usernameController.text}:${passwordController.text}");
    }

    @override
    void dispose() {
        usernameController.dispose();
        passwordController.dispose();
        super.dispose();
    }
}
