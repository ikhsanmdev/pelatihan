import 'package:flutter/material.dart';
import 'package:validators/validators.dart';

class EmailInputController extends TextEditingController {

    bool isTextEmail() => isEmail(text);
}

class PhoneInputController extends TextEditingController {

    bool isTextPhone() => isNumeric(text);
}