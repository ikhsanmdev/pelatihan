import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PlatformFunction extends StatefulWidget {
    @override
    State<StatefulWidget> createState() {
        // TODO: implement createState
        return null;
    }
}

class _PlatformFunction extends State<PlatformFunction> {

    static const platform = const MethodChannel('samples.flutter.io/battery');

    @override
    Widget build(BuildContext context) {
        return GestureDetector(
            child: Container(
                child: Center(
                    child: Text(_batteryLevel),
                ),
            ),
            onTap: () {
                _getBatteryLevel();
            },
        );
    }

    // Get battery level.
    String _batteryLevel = 'Unknown battery level.';

    Future<void> _getBatteryLevel() async {
        String batteryLevel;
        try {
            final int result = await platform.invokeMethod('getBatteryLevel');
            batteryLevel = 'Battery level at $result % .';
        } on PlatformException catch (e) {
            batteryLevel = "Failed to get battery level: '${e.message}'.";
        }

        setState(() {
            _batteryLevel = batteryLevel;
        });
    }
}