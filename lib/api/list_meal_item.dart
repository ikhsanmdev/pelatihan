import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import 'favorite_meal_model.dart';
import 'models.dart';

class ListMealItem extends StatelessWidget {

    final Meal meal;

    ListMealItem(this.meal);

    @override
    Widget build(BuildContext context) {
        return Container(
            constraints: BoxConstraints(),
            child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)
                ),
                child: Column(
                    children: <Widget>[
                        createImage(),
                        Container(
                            padding: EdgeInsets.only(top: 5, left: 16, right: 0, bottom: 8),
                            child: Row(
                                children: <Widget>[
                                    createTitles(),
                                    createFavoriteButton()
                                ],
                            )
                        ),
                    ],
                ),
            ),
        );
    }

    Widget createImage() {
        return AspectRatio(
            aspectRatio: 5 / 2,
            child: Image.network(meal.thumbnail, fit: BoxFit.fitWidth)
        );
    }

    Widget createTitles() {
        return Expanded(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                    Text(meal.mealName,
                        style: TextStyle(
                            color: Colors.indigo,
                            fontWeight: FontWeight.bold)
                    ),
                    Text(meal.category,
                        style: TextStyle(
                            color: Colors.grey[700],
                            fontWeight: FontWeight.normal)
                    )
                ],
            ),
        );
    }

    Widget createFavoriteButton() {
        return ScopedModelDescendant<FavoriteModel>(
            builder: (context, child, favorite) {
                final model = ScopedModel.of<FavoriteModel>(context, rebuildOnChange: true);
                if (favorite.isAdded(meal)) {
                    return IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                            model.remove(meal);
                        },
                    );
                } else {
                    return IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                            model.addItem(meal);
                        },
                    );
                }
            },
        );
    }
}