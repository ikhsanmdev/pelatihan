import 'package:flutter/material.dart';
import 'package:test_flutter_app/custom/input_base.dart';
import 'package:test_flutter_app/custom/input_controller.dart';

class MyAppRegistration extends StatelessWidget {

    final roundRadius = 5.0;
    final nameController = new TextEditingController();
    final phoneController = new PhoneInputController();
    final emailController = new EmailInputController();
    final usernameController = new TextEditingController();
    final passwordController = new TextEditingController();

    MyAppRegistration();

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: new AppBar(
                title: Text("Training Flutter"),
            ),
            body: SingleChildScrollView(
                child: Container(
                    color: Colors.grey,
                    padding: const EdgeInsets.only(left: 16, top: 20, right: 16, bottom: 40),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                            Container(
                                decoration: ShapeDecoration(shape: StadiumBorder(), color: Colors.blue),
                                padding: EdgeInsets.all(8),
                                margin: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                                child: Text(
                                    "Form Pendaftaran",
                                    style: TextStyle(fontSize: 24),
                                    textAlign: TextAlign.center,
                                )
                            ),
                            CustomTextField(
                                label: "Name",
                                hint: "Enter your name",
                                controller: nameController,
                                inputType: TextInputType.text),
                            CustomTextField(
                                label: "Phone",
                                hint: "Enter your phone",
                                controller: phoneController,
                                inputType: TextInputType.phone),
                            CustomTextField(
                                label: "Email",
                                hint: "Enter your email",
                                controller: emailController,
                                inputType: TextInputType.emailAddress),
                            CustomTextField(
                                label: "Username",
                                hint: "Enter your username",
                                controller: usernameController),
                            CustomTextField(
                                label: "Password",
                                hint: "Enter your password",
                                controller: passwordController,
                                isPassword: true),
                            Container(height: 20,),
                            CustomRaisedButton(
                                text: "REGISTER",
                                listener: () {
                                    _buttonTapped();
                                    Navigator.of(context).pop();
                                })
                        ],
                    ))
            ),
        );
    }

    void _buttonTapped() {
        print("Yor have tapped me!\n${usernameController.text}:${passwordController.text}");
    }
}
