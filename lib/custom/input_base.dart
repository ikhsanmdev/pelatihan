import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {

    final String label;
    final String hint;
    final TextEditingController controller;
    final TextInputType inputType;
    final bool isPassword;

    CustomTextField({
                        this.label,
                        this.hint,
                        this.controller,
                        this.inputType = TextInputType.text,
                        this.isPassword = false
                    });

    @override
    Widget build(BuildContext context) {
        return Container(
            margin: EdgeInsets.fromLTRB(0, 8, 0, 10),
            child: TextField(
                controller: controller,
                cursorColor: Colors.green,
                style: TextStyle(
                    decorationColor: Colors.indigo),
                keyboardType: (inputType == null) ? TextInputType.text : inputType,
                obscureText: isPassword,
                decoration: InputDecoration(
                    hintText: hint,
                    labelText: label,
                    labelStyle: TextStyle(color: Colors.indigo),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        gapPadding: 5
                    )
                ),
            ),
        );
    }
}

class CustomRaisedButton extends StatelessWidget {

    final String text;
    final VoidCallback listener;

    CustomRaisedButton({this.text, this.listener});

    @override
    Widget build(BuildContext context) {
        return Container(
            width: MediaQuery.of(context).size.width,
            child: RaisedButton(
                onPressed: listener,
                child: Text(text),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(8))
                ),
                highlightColor: Colors.brown,
                splashColor: Colors.green,
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
            )
        );
    }
}